/*
 ============================================================================
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Leap Year or Not in C, Ansi-style
 ============================================================================
*/
#include <stdio.h>
#include <stdlib.h>

int main() {
	char ch='A';
	printf("ASCII code %d for character %c ", ch,ch);


	int i= 0;
	for(i=0;i<=255;i++)
	{
		printf(" %3d - %c ",i,i);
		if(i%6==0)
			printf("\n");

	}
	return 0;
}



