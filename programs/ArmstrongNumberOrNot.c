/*
 ============================================================================
 Name        : ArmstrongNumberOrNot.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description :Armstrong Number Or Not.c in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

int main() {
	int num, originalvalue, numofdigits, remainder, sum = 0;
	printf("Enter input number:");
	scanf("%d", &sum);
	originalvalue = num;
	while(num != 0)
	{
		num = num / 10;
		numofdigits++;
	}
	num = originalvalue;
	while(num!=0)
	{
		remainder=num%10;
		sum=pow(remainder, numofdigits);
		num=num/10;
	}
	if(sum==originalvalue)
	{
		printf("\n %d is armstrong number", sum);
	}
	else
	{
		printf("\n %d not armstrong number", sum);
	}

	return 0;
}

