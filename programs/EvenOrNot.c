/*
 ============================================================================
 Name        : EvenOrOdd.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Even Or not in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

int main(void) {
	int a = 0;
	printf("Enter number");
	scanf("%d",&a);
	if((a % 2)==0){
		printf("Number %d is even",a);
	}else{
		printf("number %d is odd",a);
	}
	return 0;
}
