/*
 ============================================================================
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Leap Year or Not in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

int main() {
	int year; // declare imput integer
	printf("Enter the year to check whether it is leap or not: \n");
	scanf("%d",&year); // reading user input
	if((year%4 == 0 && year%100 != 0) || (year%400 == 0)) // check
		printf("%d is leap year", year); //
	else
		printf("%d is not leap rather common year ",year);
	return 0;
}


