/*
 ============================================================================
 Name        : Learn.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h> // Preprocessor statement includes header files in programs. Header files attributes and functions on program.

#include <stdlib.h>

int main(void) {
	int a,b,c;
	printf("Enter two numbers: ");
	scanf("%d %d", &a, &b);
	c = a+b;
	printf("Output: %d \n",c);
	printf("Memory location of \n a: %d \n b: %d \n c: %d", &a, &b, &c);
	return 0;
}
