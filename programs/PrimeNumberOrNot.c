/*
 ============================================================================
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Prime Number Or Not in C, Ansi-style
 ============================================================================
 */


#include <stdio.h>
#include <stdlib.h>

int main() {

	int num, i, flag=1;
	printf("Enter input number");
	scanf("%d", &num); // user input

	// running loop till num/2 find out if input number is
	// divisible by any number from 2 to num/2;
	for(i=2; i<= num/2; i++)
	{
		if(num%i==0)
		{
			flag = 0;
			break;
		}
	}
	if(flag == 1)
	{
		printf("\n%d is prime number", num);
	}else{
		printf("\n%d is not prime number", num);
	}
	return 0;
}

