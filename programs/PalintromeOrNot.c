/*
 ============================================================================
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Leap Year or Not in C, Ansi-style
 ============================================================================
*/
#include <stdio.h>
#include <stdlib.h>

int main() {
	int num, rev=0, temp, rem;
	printf("Enter input number: ");
	scanf("%d", &num);
	temp=num;
	while(num != 0)
	{
		rem = num%10;
		rev = (rev*10) + rem;
		num = num/10;
	}
	num=temp;
	if(num==rev)
	{
		printf("\n %d is palindrome number",num);
		printf("\n reversed number is %d", rev);
	}else
	{
		printf("\n %d is not palindrome number", num);
	}

	return 0;
}



